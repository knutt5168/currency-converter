package nfc.lascarelectronics.com.easylognfc;

import android.Manifest;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    // UI components
    @Bind(R.id.current_temp)
    TextView current_temp;
    @Bind(R.id.current_temp_symbol)
    TextView current_temp_symbol;
    @Bind(R.id.statusTextview)
    TextView statusTextview;
    @Bind(R.id.loggerStatus)
    TextView loggerStatus;
    @Bind(R.id.startDate)
    TextView startDate;
    @Bind(R.id.loggingRate)
    TextView loggingRate;
    @Bind(R.id.numReadings)
    TextView numReadings;
    @Bind(R.id.firmwareVersion)
    TextView firmwareVersion;
    @Bind(R.id.helpText)
    TextView helpText;
    @Bind(R.id.graphWebview)
    WebView graphWebview;
    @Bind(R.id.loadingBox)
    View loadingBox;
    @Bind(R.id.highTemp)
    TextView highTemp;
    @Bind(R.id.lowTemp)
    TextView lowTemp;

    // NFC specifics
    D14TEMP01 mLogger;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;

    // Processing data in the background
    AsyncTask DownloadData;
    AsyncTask SendSettings;

    // Graphing
    File graphFile;
    private double ch1MaxVal = -10000;
    private double ch1MinVal = 10000;

    final static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 86;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Check for available NFC Adapter
        if (!chkNFCAv()) {
            return;
        }

        // If the application was closed and an NFC intent triggered its opening
        resolveIntent(getIntent());
        mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    boolean getConfigBlock() {
        D14TEMP01.D14TempConf tagConf = null;
        try {
            tagConf = mLogger.getD14TempConf();
            loggerStatus.setText("Currently Logging: " + tagConf.isLogging);
            startDate.setText("Start Date: " + tagConf.startDate);
            loggingRate.setText("logging Rate: " + tagConf.loggingRate);
            numReadings.setText("Number of Readings: " + tagConf.sampleCount);
            firmwareVersion.setText("Firmware Version: " + tagConf.fwversion);
            current_temp.setText("" + tagConf.currentTemp);
            // todo toggleable eventually
            current_temp_symbol.setText("°C");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Performs the download operation on a background thread + informs the user of what is happening
     */
    private class DownloadData extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
            helpText.setVisibility(View.GONE);
            loadingBox.setVisibility(View.VISIBLE);
        }

        // Call after onPreExecute method
        protected String doInBackground(Void... unused) {
            double[] data;
            long start = System.currentTimeMillis();
            try {
                // See if we already have data for this logger by checking the serial number + start time
                String deviceName = mLogger.D14Config.address.replace(" ", "");
                String sessionFilename = deviceName + "_" + mLogger.D14Config.startDate.replace(":", "-") + ".txt";
                // Ensure the parent folder exists
                File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/EasyLog NFC");
                // Now check for the file
                graphFile = new File(directory.getPath(), sessionFilename);
                graphFile.getParentFile().mkdirs();
                int readingsInMemory = 0;
                 if (graphFile.exists()) {
                    // Get the number of readings stored already
                    readingsInMemory = Utilities.line_count(graphFile) - 1;
                }

                // Start building the output text
                StringBuilder printStreamOutput = new StringBuilder();

                // Only ask the logger for the readings we need
                data = mLogger.DownloadData(readingsInMemory);

                // Only print the header line when the file is created, not when being appended
                if (readingsInMemory == 0) {
                    printStreamOutput.append(deviceName).append(",");
                    printStreamOutput.append(mLogger.D14Config.startDateMillis).append(",");
                    printStreamOutput.append(mLogger.D14Config.loggingRate).append("\r\n");
                }

                // Add the new readings to the file
                for (double aData : data) {
                    printStreamOutput.append(aData).append("\r\n");
                }

                // Create, print, save, and close the text file
                PrintStream text_output = new PrintStream(new FileOutputStream(graphFile, true), false, "Cp1252");
                text_output.print(printStreamOutput.toString());
                text_output.flush();
                text_output.close();

                long end = System.currentTimeMillis();
                return ("Received " + data.length + " readings in " + ((end - start) / 1000) + " seconds\r\n\n\n" + printStreamOutput.toString());

            } catch (Exception e) {
                e.printStackTrace();
                return ("");
            }
        }

        protected void onPostExecute(final String outcome) {
            // todo Pass to graph
            if (outcome.equals("")) {
                loadingBox.setVisibility(View.GONE);
                helpText.setVisibility(View.VISIBLE);
                helpText.setText("Something went wrong,please try again");
            } else {
                // Success! Build the graph
                buildGraph();
            }
        }
    }

    void buildGraph() {
        // Webview settings
        graphWebview.setBackgroundColor(getResources().getColor(R.color.transparent));
        graphWebview.setInitialScale(1);
        graphWebview.getSettings().setJavaScriptEnabled(true);
        graphWebview.getSettings().setLoadWithOverviewMode(true);
        graphWebview.getSettings().setUseWideViewPort(true);
        graphWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        graphWebview.setVerticalScrollBarEnabled(false);
        graphWebview.setHorizontalScrollBarEnabled(false);
        graphWebview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;
            }
        });
        graphWebview.addJavascriptInterface(new AndroidJS(), "AndroidJS");
        graphWebview.loadUrl("file:///android_asset/graph.htm");
    }

    private class AndroidJS {
        @JavascriptInterface
        public void hide_progress_bar() {
            runOnUiThread(new Runnable() {
                public void run() {
                    // Make the graph visible
                    graphWebview.setVisibility(View.VISIBLE);
                    loadingBox.setVisibility(View.GONE);
                    // Set the min + max values
                    // todo Celsius will be toggle-able
                    highTemp.setText("" + ch1MaxVal + "°C");
                    lowTemp.setText("" + ch1MinVal + "°C");
                }
            });
        }

        @JavascriptInterface
        public String getAxis1Symbol() {
            // Highcharts needs sample rate in ms
            // todo toggle in the future
            return "°C";
        }

        @JavascriptInterface
        public long get_sample_rate() {
            // Highcharts needs sample rate in ms
            return mLogger.D14Config.loggingRate;
        }

        @JavascriptInterface
        public double get_start_time() {
            return mLogger.D14Config.startDateMillis;
        }

        @JavascriptInterface
        public String getaxis1Data() {
            // Get data from text file
            FileInputStream fin = null;
            double value;
            try {
                // Open the text file
                fin = new FileInputStream(graphFile);
                BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
                StringBuilder sb = new StringBuilder();
                boolean skipLine = true;
                // Skipping the header line
                String line = reader.readLine();
                // Loop through all available readings
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append(",");
                    // Checking for min/max values while we're at it
                    value = Double.valueOf(line);
                    if (value >= ch1MaxVal) {
                        ch1MaxVal = value;
                    }
                    if (value <= ch1MinVal) {
                        ch1MinVal = value;
                    }
                }

                // Cut off the last comma so it fits highstocks JSON formatting
                sb.setLength(sb.length() - 1);
                //Make sure you close all streams.
                fin.close();

                // Return the formatted string
                return sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }
    }

//    @OnClick(R.id.sample)
//    public void sample() {
//        try {
//            if (mLogger.writeRegister("1000", (byte) 0)) {
//                mOutput.setText("Sample bit cleared");
//            } else {
//                mOutput.setText("Sample bit clearing failed");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//            mOutput.setText("Sample bit clearing failed");
//        }
//    }

    /**
     * Receives Android broadcasts that are sent went an NFC tag is nearby
     *
     * @param intent
     */
    private void resolveIntent(Intent intent) {
        String action = intent.getAction();
        if ("android.nfc.action.TAG_DISCOVERED".equals(action) || "android.nfc.action.TECH_DISCOVERED".equals(action)) {
            try {
                // Init the custom logger class if needed
                if(mLogger == null){
                    mLogger = new D14TEMP01();
                }
                if (mLogger.isValidTag(intent.getParcelableExtra(NfcAdapter.EXTRA_TAG))) {
                    // If the config block comes back OK then carry on with the user-selected task
                    if (getConfigBlock()) {
                        // todo let the user choose to setup as well
                        DownloadData = new DownloadData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
                    } else {
                        statusTextview.setText("Failed to download the loggers information, please attempt connection again");
                    }
                } else {
                    statusTextview.setText("An invalid tag tried to connect");
                }
            } catch (Exception e) {
                statusTextview.setText(R.string.noTagFound);
            }
            statusTextview.setText("Connected to something");
        }
    }

    /**
     * Ensure our app is the top NFC priority... and that NFC is turned on
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mNfcAdapter != null) {
            if (mNfcAdapter.isEnabled()) {
                // Foreground dispatch gives priority over other applications when the app is opened. So you don't have to move your phone to select the right app
                mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
            } else {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(MainActivity.this);
                alertbox.setTitle("Info");
                alertbox.setMessage("You must activate NFC on your device to continue");
                alertbox.setPositiveButton("Turn On", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                            startActivity(intent);
                        }
                    }
                });
                alertbox.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertbox.show();
            }
        }
    }

    /**
     * Called when the application is no longer the most visible item to the user. We no longer need to be the top NFC priority
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
        }
    }

    /**
     * Called whenever the tag is in proximity for the first time
     *
     * @param intent
     */
    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    /**
     * Checks for NFC availability on this device
     *
     * @return True / false if available
     */
    public boolean chkNFCAv() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
            finish();
            return false;
        }
        return true;
    }
}
