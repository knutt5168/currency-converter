package nfc.lascarelectronics.com.easylognfc;

import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by blaw on 2/5/2016.
 */
public class D14TEMP01 {
    // NFC top objects
    NfcA nfcATag;
    Tag mTag;

    // Variables from documentation
    double offsetZeroC = 0.08125d;
    double gainPc = 2.9861E-4d;
    D14TempConf D14Config;

    // Other
    DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

    public double[] convertToTemperature(byte[] raw) {
        double[] vals = new double[(raw.length / 2)];
        for (int i = 0; i < raw.length; i += 2) {
            vals[i / 2] = ((((D14Config.refCalibrated / 32767.0d) * ((double) (((raw[i] & 255) * 256) + (raw[i + 1] & 255)))) / 8.0d) - this.offsetZeroC) / this.gainPc;
            vals[i / 2] = (double) Math.round(vals[i / 2] * 100) / 100;
        }
        return vals;
    }
    

    public int getIntFrom2Bytes(byte bh, byte bl) {
        return ((bh & 255) * 256) + (bl & 255);
    }

    public byte[] readMemoryArea(String hAddr, int tcnt) throws IOException {
        byte bsize = (byte) 16;
        // Make sure the connection is stable/available
        if (!selectTag()) {
            return null;
        }

        byte[] vals = new byte[tcnt];
        int index = 0;
        int cyc = 0;
        while (cyc < tcnt - 1) {
            byte bcnt;
            if (cyc < tcnt - (bsize - 1)) {
                bcnt = bsize;
            } else {
                bcnt = (byte) (tcnt - cyc);
            }
            byte[] resp = readRegisters(hAddr, bcnt);
            if (hAddr != null) {
                hAddr = null;
            }
            byte i = (byte) 0;
            int index2 = index;
            while (i < bcnt) {
                index = index2 + 1;
                vals[index2] = resp[i];
                i++;
                index2 = index;
            }
            cyc += bsize;
            index = index2;
        }
        return vals;
    }

    public byte[] readRegisters(String haddr, byte cnt) throws IOException {
        byte[] resp2 = new byte[cnt];
        byte bnum = (byte) 1;
        if (!selectTag()) {
            return null;
        }
        byte[] addr;
        byte[] cmd;
        if (haddr != null) {
            addr = hexStringToByteArray(haddr);
            cmd = new byte[6];
            cmd[0] = (byte) -94;
            cmd[2] = addr[0];
            cmd[3] = addr[1];
            nfcATag.transceive(cmd);
        }
        return nfcATag.transceive(new byte[]{(byte) 48, cnt});
    }

    /**
     * Converts an input hex string into its byte array equivalent
     *
     * @param s
     * @return
     */
    public byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[(len / 2)];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * Attempts to open a connection with the nearby tag
     *
     * @return true / false if the connection was made
     */
    boolean selectTag() {
        try {
            if (nfcATag == null) {
                nfcATag = NfcA.get(mTag);
                return true;
            } else if (nfcATag.isConnected()) {
                return true;
            } else {
                nfcATag.connect();
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public double[] DownloadData(int startPosition) throws IOException {
        if (selectTag()) {
            byte[] resp;

            // Calculate the starting position
            // Memory bank starts at location A002
            // Start position doubled since each reading is 2 bytes
            int startingValue = 0xA002 + startPosition * 2;

            if(startPosition < 0){
                startPosition = 0;
            }
            resp = readMemoryArea(Integer.toHexString(startingValue), (D14Config.sampleCount - startPosition) * 2);
            if (resp == null) {
                return null;
            }

            double[] vals = convertToTemperature(resp);
            long[] startTime = new long[2];
            D14Config = getD14TempConf();
            startTime[0] = D14Config.startLog.getTime().getTime();
            startTime[1] = (long) (D14Config.sampleCount * 1000);
//                Intent intent = new Intent(this.context, GraphDrawActivity.class);
//                intent.putExtra("type", "line");
//                intent.putExtra("Temp_data", vals);
//                intent.putExtra("Temp_start", startTime);
//                this.context.startActivityForResult(intent, 10)
            return vals;
        }
        return null;

    }

    public class D14TempConf {
        boolean batEnabled;
        boolean batLow;
        String address;
        boolean extInput;
        byte fwversion;
        int gpioSelect;
        int sampleCount;
        int loggingRate;
        int hours;
        int mins;
        int secs;
        String startDate;
        long startDateMillis;
        boolean isLogging;
        String spiCommand;
        int spiMode;
        Calendar startLog;
        int calibRefReg = 19;
        double refCalibrated = 1.1935d;
        double currentTemp;
        public D14TempConf() {
            this.fwversion = (byte) 4;
        }
    }

    public byte[] getControlRegs() throws IOException {
        byte[] resp = new byte[16];
        for (int i = 0; i < 16; i++) {
            resp[i] = readRegisters(String.format("%04x", i + 4096), (byte) 1)[0];
        }
        return resp;
    }

    /**
     * Attains some basic information about the NFC tag, and determines if it is one of ours
     *
     * @param p
     * @return
     * @throws IOException
     */
    public boolean isValidTag(Parcelable p) throws IOException {
        boolean isNfcA = false;
        mTag = (Tag) p;
        byte[] id = mTag.getId();

        String prefix = "android.nfc.tech.";
        for (String tech : mTag.getTechList()) {
            if (tech.substring(prefix.length()).equals("NfcA")) {
                isNfcA = true;
            }
        }

        if (isNfcA) {
            if (id[0] == 80) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public double d14TempImmediateRead() throws IOException {
        byte[] adcin = readRegisters("100A", (byte) 1);
        writeRegister("100A", (byte) ((D14Config.calibRefReg << 2) | 128));
        writeRegister("1009", (byte) 48);
        writeRegister("100D", (byte) 64);
        writeRegister("1009", (byte) 49);
        byte[] stat = readRegisters("1009", (byte) 1);
        while (stat[0] != 51) {
            stat = readRegisters("1009", (byte) 1);
        }
        writeRegister("1009", (byte) 48);
        writeRegister("100A", adcin[0]);
        byte[] raw = new byte[2];
        int i0 = ((readRegisters("1012", (byte) 1)[0] << 16) + ((readRegisters("1011", (byte) 1)[0] << 8) & 65280)) + (readRegisters("1010", (byte) 1)[0] & 255);
        raw[0] = (byte) ((i0 >> 13) & 255);
        raw[1] = (byte) ((i0 >> 5) & 255);
        return convertToTemperature(raw)[0];
    }

    public boolean writeRegister(String haddr, byte data) throws IOException {
        if (!selectTag()) {
            return false;
        }
        byte[] addr;
        byte[] cmd;
        addr = hexStringToByteArray(haddr);
        cmd = new byte[6];
        cmd[0] = (byte) -94;
        cmd[1] = (byte) 8;
        cmd[2] = addr[0];
        cmd[3] = addr[1];
        cmd[4] = data;
        nfcATag.transceive(cmd);
        return true;
    }

    public D14TempConf getD14TempConf() throws IOException {
        if (selectTag()) {
            D14Config = new D14TempConf();
            byte[] resp = getControlRegs();
            D14Config.address = Utilities.getHex(mTag.getId());
            D14Config.batEnabled = (resp[2] & 128) != 0;
            D14Config.batLow = (resp[2] & 64) != 0;
            D14Config.isLogging = (resp[0] & 64) != 0;
            D14Config.sampleCount = getIntFrom2Bytes(resp[12], resp[11]);
            D14Config.hours = D14Config.sampleCount / 3600;
            D14Config.mins = (D14Config.sampleCount - (D14Config.hours * 3600)) / 60;
            D14Config.secs = (D14Config.sampleCount - (D14Config.hours * 3600)) % 60;
            resp = readMemoryArea("FF00", 64);
            D14Config.startLog = Calendar.getInstance();
            D14Config.startLog.set(getIntFrom2Bytes(resp[1], resp[0]), getIntFrom2Bytes(resp[3], resp[2]) - 1, getIntFrom2Bytes(resp[7], resp[6]), getIntFrom2Bytes(resp[9], resp[8]), getIntFrom2Bytes(resp[11], resp[10]), getIntFrom2Bytes(resp[13], resp[12]));
            D14Config.startDate = dateFormat.format(D14Config.startLog.getTime());
            D14Config.startDateMillis = D14Config.startLog.getTimeInMillis();
            D14Config.loggingRate = getIntFrom2Bytes(resp[24], resp[23]);

            D14Config.extInput = (resp[26] & 3) != 0;
            D14Config.spiCommand = "";
            for (byte i = (byte) 0; i < resp[32]; i++) {
                D14TempConf d14TempConf = D14Config;
                d14TempConf.spiCommand += String.format("%02x ", resp[(i + 32) + 1]);
            }
            if (D14Config.spiCommand.length() == 0) {
                D14Config.spiCommand = null;
            }
            D14Config.spiMode = ((resp[37] & 240) - 128) / 16;
            if ((resp[38] & MotionEventCompat.ACTION_MASK) == 21) {
                D14Config.gpioSelect = 1;
            } else {
                D14Config.gpioSelect = 0;
            }

            resp = readMemoryArea("FF40", 3);
            D14Config.calibRefReg = resp[0];
            D14Config.refCalibrated = ((double) getIntFrom2Bytes(resp[2], resp[1])) / 10000.0d;

            D14Config.currentTemp = d14TempImmediateRead();
            return D14Config;
        }
        return null;
    }
}
