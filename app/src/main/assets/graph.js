var chart;
var first_axis_symbol = AndroidJS.getAxis1Symbol();
var sample_rate = AndroidJS.get_sample_rate();
var start_time = AndroidJS.get_start_time();

$(function () {
    chart = new Highcharts.StockChart({
        credits: {
            enabled: false
        },
        dateTimeLabelFormats: {
            second: '%l:%M:%S %p',
            minute: '%l:%M:%S %p',
            hour:   '%l:%M:%S %p',
            day:    '%b-%d-%l:%M:%S %p',
            week:   '%b-%d-%l:%M:%S %p',
            month:  '%b-%d-%l:%M:%S %p',
            year:   '%b-%d-%l:%M:%S %p'
        },
         title: {
            text: "",
            style: {
                fontWeight: 'bold',
           }
        },
        chart: {
            events: {
                load: function(event) {
                    AndroidJS.hide_progress_bar();
                }
            },
            marginTop: 20,
            marginBottom: 75,
            renderTo: 'container',
            backgroundColor:null,
            pinchType: 'x',
            alignTicks: false,
            resetZoomButton: {
                theme: {
                    fill: '#3ae',
                    r: 4,
                    style: {
                        color: '#fff',
                    }
                }
            }
        },
        subtitle: {
            text: ''
        },
        rangeSelector: {
            enabled: false
        },
        navigator: {
            enabled: false,
        },
        scrollbar: {
            enabled: false,
        },
        xAxis: {
            type: 'datetime',
            labels: {
                overflow: 'justify',
                staggerLines: 2,
                style: {
                    color: 'white',
                },
            },
            dateTimeLabelFormats: {
                second: '%l:%M:%S %p',
                minute: '%l:%M %p',
                hour:   '%l %p',
                day:    '%b-%d-%l %p',
                week:   '%b-%d-%l %p',
                month:  '%b-%d-%l %p',
                year:   '%b-%d-%l %p'
            },
            gridLineColor: '#9A9',
            gridLineWidth: 1,
        },
        yAxis: { // Primary yAxis
            showEmpty: false,
            gridLineColor: '#9A9',
            showLastLabel: true,
            minTickInterval: .5,
            opposite: false,
            labels: {
                formatter: function() {
                    return this.value + first_axis_symbol;
                },
                style: {
                    color: 'white',
                },
                align: 'right',
                position: 'outside',
                x: -5
            },
        },
        plotOptions: {
            series: {
                pointStart: start_time,
                pointInterval: sample_rate,
                connectNulls: true,
                animation: false,
                shadow: false,
                marker: {
                    enabled: false
                }
            },
            line: {
                dataGrouping: {
                   approximation: "average",
                   dateTimeLabelFormats: {
                       millisecond: ['%b %e, %l:%M:%S %p', '%b %e, %l:%M:%S', '-%l:%M:%S %p'],
                       second: ['%b %e, %l:%M:%S %p', '%b %e, %l:%M:%S', '-%l:%M:%S %p'],
                       minute: ['%b %e, %l:%M %p', '%b %e, %l', '-%l:%M %p'],
                       hour: ['%b %e, %l:%M %p', '%b %e, %l', '-%l:%M %p'],
                       day: ['%b %e, %Y', '%b %e', '-%b %e, %Y'],
                       week: ['Week from %b %e, %Y', '%b %e', '-%b %e, %Y'],
                       month: ['%B %Y', '%B', '-%B %Y'],
                       year: ['%Y', '%Y', '-%Y']
                    }
                },
                lineWidth: 2,
                marker: {
                   enabled: false,
                   states: {
                      hover: {
                         enabled: false,
                         radius: 5
                      }
                   }
                },
                shadow: false,
                states: {
                   hover: {
                      lineWidth: 2
                   }
                }
            }
        },
        tooltip: {
            useHTML: true,
            animation: false,
            borderWidth: 0,
            backgroundColor:"rgba(255, 255, 255, 0)",
            borderRadius: 0,
            shadow: false,
            xDateFormat: '%b %e, %l:%M:%S %p',
            valueDecimals: 2,
            shared: true,
            headerFormat: '<center>{point.key}</center>',
            pointFormat: ' <span style="color:{series.color}">{series.name}</span>: {point.y} ',
            crosshairs: {
                dashStyle: 'dash'
            },
            positioner: function (labelWidth, labelHeight, point) {
                return { x: (($('#container').width()/2)-(labelWidth/2)), y: $('#container').height() - 47};
            },
        },
        legend: {
            enabled: false,
        },
        series: [{
            tooltip: {
                valueSuffix: first_axis_symbol
            },
            color: "#EF4E50",
            data: JSON.parse("[" + AndroidJS.getaxis1Data() + "]")
        }]
    });
});